
/*
 * Escrito por Miguel Angel Gomez 19/12/2022
 Proyecto pensado para leer desde la conexion serie de un indicador de balanza,
 o con un apmlificador hx711
 visualizar en LCD 16x2 o 20x4
 almacenar datos en memoria SD
 e Imprimir en impresora termica

Hardware utilizado:(-libreria)
Arduino Mega
lcd 44780 compatible - LiquidCrystal
lcd 44780 I2C driver -
Teclado de membrana 4x4 -Keypad   -Key
Modulo RTC 3231 i2c -DS3231
modulo Sd card SPI  -SD
MAX 232  //para la conexion con el indicador de balanza

*/


#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include <PPLA_Printer.h>
#include <TimeLib.h>
/*  Agregar la linea
      void setIICAddress(uint8_t add);

    despues de la linea 57 del archivo LiquidCrystal_I2C.h
    de la carpeta LiquidCrystal_I2C dentro de las librerias en el directorio de Arduino

  Agegar el codigo:
      void LiquidCrystal_I2C::setIICAddress(uint8_t add)
    {
      _Addr = add;
    }

  despues de la linea 54 del archivo LiquidCrystal_I2C.cpp
  dentro de las librerias en el directorio de Arduino

 */

#include <SD.h>
#include <DS3231.h>
#include <HX711_ADC.h>
#include <Keypad.h>
#include <Key.h>
#include <SoftPWM.h>
#include <SoftPWM_timer.h>
#include <EEPROM.h>
#include <TimerOne.h>
#include <PPLA_Printer.h>

/***
Esquema de la EEPROM
dir   tamaño  Dato
9     char    lcd_led_pow
10    float   calValue
14    long    tareOffset
20    char    modoLectura
21    int     ultimo numeroTurno
23    int     ultimo numeroOperador
25    10*char clave alfanumerica(clave)
40    float   precision
44    Float   cargaMax
48    2*char  strUnit
50    long    baudrateS1
55    long    ultimaSecuencia
60    byte    Lcd I2c Address
61    byte    Rtc I2c Address
62    char    pesoDecs
64    Float   autoZeroLimit(%)

***/

/***Pines utilizados
 *LCD direct
 *  led 34
 *  RS  23
    EN  25
    D4  27
    D5  28
    D6  29
    D7  30
 *LCD direct
 *  led 34
 *  SCK 20
 *  SDA 21
 *
 *kEYPAD
 *  A0
 *  A1
 *  A2
 *  A3
 *  A4
 *  A5
 *  A6
 *  A7
 *
 *hx711
 *  dOUT  40
 *  dCLK  42
 *
 *sDcARD
 *  PinSel 4
 *  SPI PORT
 *    50
 *    51
 *    52
 *    53
 *
 *Impresora
 *  Tx2
 *  Rx2
 *
 *Lector Indicador
 *  Tx1
 *  Rx1
 *
 *Comunicacion PC
 *  Tx0
 *  Rx0
 *
 * Modulo Bluetooth
 *  Tx3
 *  Rx3
 *  State 39
 *  Modo  41
 ***/


//Display LCD
#define LCD_MODE_I2C   //comentar esta linea para usar el LCD directamente
#define LONG_RENGLON 20 //tamaño del largo del Display
#define CANT_RENGLONES 4 //cantidad de renglones del Dislpay
#define addLcdI2cAddress 60 //
#define lcd_led  34 //pin donde se conecta el pin l+ del display(si se usa un display grande abra qie conectar un transistor)
#define add_lcd_led_pow  9
unsigned char lcd_led_pow = 50;//potencia de iluminacion del backlight del display
//declaracion de pines del display

#define RS 23
#define EN 25
#define D4 27
#define D5 28
#define D6 29
#define D7 30
int lcdMode = 0;
byte LcdI2cAddress=0;
#ifdef LCD_MODE_I2C
LiquidCrystal_I2C lcd(0x27,LONG_RENGLON, CANT_RENGLONES);
#else
LiquidCrystal lcd(RS,EN,D4,D5,D6,D7);
#endif
//extern LiquidCrystal lcd;
//extern LiquidCrystal_I2C lcd;
//maquina de estados
int modo = 1;
int prevModo=1;
char modoAnt=0;
boolean firstTime=0;
String textoStr="0";
char error = 0;
boolean Aceptar=0;
boolean Cancelar=0;
boolean Asterisco=0;
boolean Hash=0;
int indice_conf=0;
#define opcionesConf_len 16 //Aclarar cuantas opciones se escriben abajo
String opcionesConf[opcionesConf_len]={ "Cambiar Turno","Cambiar Operador", "Cambiar Clave","Calibracion",
"Precision", "Carga Maxima", "Modo Lectura", "Unidades", "Actualizar Hora", "Actualizar Fecha", "Baudrate Serie" ,
" Tara de la balanza", "Direccion I2C LCD", "Direccion I2C RTC", "Brillo Display LCD", "Porcentaje AutoZero"};
int indice_confAnt = 0;


//Variables para la creacion del Ticket
unsigned char pesoDecs=2; //variable para guardar la cantidad de decimales del peso a mostrar
#define addPesoDecs 62//donde se almacena la configuracion de decimales en EEPROM
#define DIGITOS_PESO 7  //los lugares a guardar para mostrar el peso en el LCD, comprende la coma
String opID="";
int numeroProd=-1; //numero de producto en la tabla prods.txt
String nombreProd=""; //Nombre completo del producto
String nickProd="";   //nombre corto del producto
String RNPA="";  //Numero RNPA del producto
String renglon="";  //String con todos los datos del producto (Numero)
long numeroLote=0;
int numeroLoteAnt=0;
String numeroLoteStr="";
long secuencia=0;
long ultimaSecuencia=0;
#define addUltimaSecuencia 55
String fechaElabStr="";
String fechaVencStr="";

///keypad variables///
boolean hold = 0;
const char columnas = 4;
const char filas = 4;
unsigned char teclas[filas][columnas] = {{'1', '2', '3', 'A'}, {'4', '5', '6', 'B'},{'7', '8', '9', 'C'}, {'*', '0', '#', 'D'} }; //pines usados
char pinesfilas[filas] = {A7, A6, A5, A4};
char pinescolumnas[columnas] = {A3, A2, A1, A0};
char key = 0;
Keypad Teclado(makeKeymap(teclas), pinesfilas, pinescolumnas, filas, columnas);

//Variables para Modo lectura Celda
#define addModoLectura  20 //direccion del eeprom donde se guarda la variable modo lectura
#define MODO_CELDA  10
#define MODO_SERIE 20
char modoLectura=0; //10=celda(HX711)  -- 20=Puerto Serie
const char addOffset = 15;  //Primera Direccion donde se guarda el offset de las celdas
const char addCalVal= 10;     //direccion del primer valor de calibracion
#define autoZeroMaxLimit 1.5  //porcentaje mayor a setear para autozero
#define addAutoZeroLimit 64 //direccion donde se almacena el autozero
float autoZeroLimit = 0;  //Porcentaje para hacer autozero
float pesoAutoZero = 0; //peso por debajo del cual se hace el autozero (cargaMax*autoZeroLimit)
//pines hx711
#define  hx711_pin_dout 40 //numero de pin donde se conecta el pin dout del hx711
#define hx711_pin_sck 42   //numero de pin donde se conecta el pin sck del hx711_pin_sck
HX711_ADC celda(hx711_pin_dout,hx711_pin_sck);  //se declara el objeto celda (amplificador)
float peso = 0.0;   //peso que vendra leido de la celda
long oldTareOffset=0;
long tareOffset;    //variable donde se almacena el offset
float calValue=0;   //variable donde se almacena el factor de calibracion
float pesoIndicado = 0;  //variable donde se almacena temporariamente el dato que se ingresa desde el puerto serie para la Calibracion
float precision=0;
const float precDefault = 0.01; //Se define la precision por defecto en 0.01
char addPrecision=40;
float cargaMax=100;
char addCargaMax=44;
char addStrUnit=48;
String strUnit="";


//Variables para el modo entrada serial

long baudrateS1=9600;//se establece la velocidad del puerto segun el indicador a leer
char addBaudrateS1=50;

//variables para el TimerOne
int dSecCount = 0;  //variable que se incrementa cada 100mS
boolean oneSec = 0; //variable que se activa cada 1 segundo
boolean oneDSec = 0;  //variable que se activa cada 100mS


//Variables para la sdCard
File myfile;//declaracion del archivo que se usa para trabajar sobre la tarjeta de memoria
#define sdCsPin 4
//Variables de datos del TICKET
String codeId = ""; //codigo ID fecha sin barras
#define add_numeroTurno 21
unsigned int numeroTurno=0;
String nombreTurno;
#define add_numeroOperador 23
int numeroOperador=0;
String nombreOperador;
#define add_clave 25
String clave; //largo de 3 caracteres
#define largoClave 3 //definicion largo de la clave

//Variables para el RTC
//pines i2c Sda-D 20  Sck-C 21
#define addRtcI2cAddress 61
byte RtcI2cAddress=0;
DS3231 setterClock;
RTClib Clock;
DateTime Now;

//impresora
PPLA_Printer printer(&Serial2);

//Variables para Bluetooth HC-06
#define HC06State 39
#define HC06Mode 41
int ATMode =0;//modo AT del HC06
void setup() {
//Inicializacion de los puertos series
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println(F("Programa de gestion de balanzas\nIniciando..."));
  //Iniciando impresora
  Serial2.begin(9600);
  delay(5000);
  config_printer();
  //DS3231M.temperature(); //por si sirve la temperatura
  SD.begin(sdCsPin);//Se inicia el modulo SD con cs conectado en pin 4
  Serial.println(F("Iniciada SD card"));
  Wire.begin(); //Iniciando comunicacion I2C
  EEPROM.get(addRtcI2cAddress, RtcI2cAddress);
  EEPROM.get(addLcdI2cAddress, LcdI2cAddress);
  if (LcdI2cAddress ==0){
    LcdI2cAddress = 0x27;
  }
  Serial.println(F("Iniciada Comunicacion I2C"));
  for (byte address = 1; address < 127; ++address) {  //escaneo de dispositivos I2C
    Wire.beginTransmission(address);
    byte error = Wire.endTransmission();
    if (error == 0) { //Se imprimen los dispositivos encontrados
      if (address == RtcI2cAddress){  //si la direccion es la que esta configurada el Rtc
        Serial.println("Encontrado modulo Ds3231");
      }else if(address == LcdI2cAddress) {  //si la direccion encontrada es la que usa el Lcd
        Serial.println("Encontrado modulo LCD");
      }else{
        Serial.print("Dispositivo encontrado en la direccion ");
        if (address < 10) {
          Serial.print("0");
        }
        Serial.println(address);
      }
    }
  }

  #ifdef LCD_MODE_I2C
  lcd.setIICAddress(LcdI2cAddress);
  lcd.begin( CANT_RENGLONES,LONG_RENGLON);
  lcd.backlight();
  #else
  lcd.begin( CANT_RENGLONES, LONG_RENGLON); //se inicializa el Display
  #endif

  lcd.init();
  lcd.print(centerLcdString(F("_Balanza C J_"),LONG_RENGLON ));
  lcd.setCursor(0, 1);
  lcd.print(completeLcdString(F("Iniciando ..."), LONG_RENGLON));
  pinMode(lcd_led, OUTPUT); //se declara como salida el pin del backlight del display
  Timer1.initialize(10000);// se inicia el timer que interrumpe cada 100mS
  Timer1.attachInterrupt(countDSecCount);   //se conecta la interrupcion del timer a la funcion

  SoftPWMBegin(); //se inicia el pwm por software
  SoftPWMSet(lcd_led, 0); //se establece el pin de salida
  SoftPWMSetFadeTime(lcd_led, 1000, 1000); //se configura el tiempo de trabajo del pwm
  EEPROM.get(add_lcd_led_pow,lcd_led_pow); //Se rescata el ultimo valor seteado del baclight
  SoftPWMSet(lcd_led, lcd_led_pow); //se inicia la salida por el pin indicado a la potencia guardada

  Teclado.addEventListener(keypadEvent);
  Teclado.setHoldTime(1000);
  Teclado.setDebounceTime(100);
  strUnit += char(EEPROM.read(addStrUnit)); //se recuperan las unidades de la eeprom
  strUnit += char(EEPROM.read(addStrUnit + 1));
  //se establece la precision y las unidades seteadas
  EEPROM.get(addPrecision,precision); //se recupera la precision de la eeprom
  EEPROM.get(addCargaMax, cargaMax);  //se recupera la carga max de la eeprom
  if (precision < 0   ) precision = precDefault;//si la precision seteada es incorrecta se establece la de defecto
  Serial.print(F("La precision establecida es de "));
  Serial.print(precision);
  Serial.print(strUnit);
  Serial.print(F(".\nLa carga maxima admitida es de "));
  Serial.print(cargaMax);
  Serial.print(strUnit);
  Serial.print(F(".\n"));

  EEPROM.get(addModoLectura,modoLectura);
  if (modoLectura == MODO_CELDA){
    lcd.setCursor(0,2);
    lcd.print(completeLcdString(F("Iniciando Celda..."),LONG_RENGLON));
    Serial.println(F("Inciando Configuracion de Celda..."));
    celda.begin();
    delay(1000);

    EEPROM.get(addOffset, tareOffset);
    Serial.print(F("TareOffset en EEPROM:"));
    Serial.println(tareOffset);
    celda.setSamplesInUse(32);// 2,4, 8, 16, 32, 64, 128
    celda.start(1500, false);
    if (celda.getTareTimeoutFlag()) {
      lcd.setCursor(0, 2);
      lcd.print(completeLcdString(F("  Falla Celda       "),LONG_RENGLON));
      delay(5000);
    } else {
      EEPROM.get(addCalVal, calValue);
      celda.setCalFactor(calValue);
      celda.setTareOffset(tareOffset);
      Serial.print(F("TareOffSet ajustado: "));
      Serial.println(celda.getTareOffset());
      Serial.print("Coeficiente: ");
      Serial.println(String(calValue));
      EEPROM.get(addAutoZeroLimit, autoZeroLimit);
      pesoAutoZero = cargaMax/100*autoZeroLimit;
      Serial.print(F("Peso auto Cero: "));
      Serial.print(pesoAutoZero);
      Serial.println(strUnit);
      lcd.setCursor(15,2);
      lcd.print(" OK");
      Serial.println(F("Modo Celda OK"));

    }
  }else { //modo lectura de Indicador via serie 232 (MAX 232)
    EEPROM.get(addBaudrateS1,baudrateS1); //se recupera el baudrate guardado en eeprom
    Serial1.begin(baudrateS1); //se inicia el puerto Serie 1 para recibir del Indicador
    Serial1.setTimeout(200); //se termina la recepcion serial despues de 200ms
    while(!Serial1); // wait for serial port to connect. Needed for native USB port only
    Serial.println(F("Iniciado Modo Puerto Serie 1 OK"));
    Serial.print(F("Baudrate: "));
    Serial.println(baudrateS1);
    lcd.setCursor(0,2);
    lcd.print(completeLcdString(F("Recibiendo Indicador"),LONG_RENGLON));
    delay(1500);
  }
  //Inicializacion de las variables del TICKET
  // se recupera el ultimo Turno usado
  EEPROM.get(add_numeroTurno, numeroTurno );
  nombreTurno=getNombreTurno(numeroTurno);
  Serial.print(F("Turno vigente: "));
  Serial.println(nombreTurno);
// se recupera nombre del ultimo operador
  EEPROM.get(add_numeroOperador, numeroOperador);
  nombreOperador=getNombreOperador(numeroOperador);
  Serial.print(F("Operador vigente: "));
  Serial.println(nombreOperador);
// Se recupera la ultima clave usada guardada en eeprom
  clave="";
  for(int n=0; n<largoClave ; n++){
    char c=EEPROM.read(add_clave+n);
    clave+=c;
  }
  EEPROM.get(addUltimaSecuencia, ultimaSecuencia);
  Serial.print(F("Ultimo Ticket: "));
  Serial.println(ultimaSecuencia);

  Serial.print(F("La clave MFR es:"));
  Serial.println(clave);

  Now = Clock.now();
  Serial.println(fechaString(Now.day(), Now.month(), Now.year()));
  lcd.setCursor(0,3);
  lcd.print(fechaString(Now.day(), Now.month(), Now.year()));
  lcdPrintSpaces(2);
  Serial.println(horaString(Now.hour(), Now.minute(), Now.second()));
  lcd.print(horaString(Now.hour(), Now.minute(), Now.second()));

  Serial.println(F("Ingrese 'CONF' para entrar en modo Configuracion")) ;
  Serial.println(F("Opciones: 'Aceptar' 'Conf' 'Tara'"));
  delay(2000);
//Probando estado de modulo Bluetooth
  Serial3.begin(38400);
  Serial3.println("AT");
  delay(200);
  if (Serial3.available()){//si hay datos para leer del HC06
    String in=Serial3.readString();//se almacena en in
    in.trim();//se eliminian los espacios
    if (in == "OK"){  // si la respuesta es OK
      ATMode=1; //se entra en modo de configuracion del Modulo Bluetooth
      lcd.setCursor(0,0);
      lcd.print(centerLcdString(F("HC-06 AT CONF"),20));
      lcd.setCursor(0,1);
      lcd.print(F("<                   "));
      lcd.setCursor(0,2);
      lcd.print(F(">                   "));
      lcd.setCursor(0,3);
      lcd.print(F("Reinicie para salir "));
    }else{//Si la respuesta es distinta de OK
      ATMode=2;  //el modulo esta en modo AT pero hay algun error
    }
  }else{//si no hay respuesta
    ATMode = 0;//El modulo no esta en modo AT,
    Serial3.begin(9600); //se reinicia la puerta serial 3 a 9600 para modo comunicacion
    }
//Entrando en modo de configuracion de Modulo HC-06
  while(ATMode){
    if (Serial.available()){
      String in = Serial.readString();
      in.trim();
      Serial3.println(in);
      lcd.setCursor(1,1);
      lcd.print(in);
      lcd.print("             ");
    }
    if (Serial3.available()){
      String In = Serial3.readString();
      In.trim();
      Serial.println(In);
      lcd.setCursor(1,2);
      lcd.print(In);
      lcd.print("                   ");
    }
  }


}

void loop() {
  key = Teclado.getKey(); //se realiza el escaneo del teclado de membrana

  //variable firstTime para saber cuando se produce un cambio de estado
  if (modoAnt != modo){
    firstTime=true;
  }else{
    firstTime=false;
  }
  modoAnt=modo;

  if (modoLectura == MODO_CELDA){ //si se trabaja en modo Lectura desde celda de carga
    //celda.update(); //se realiza la medicion de la celda
    peso=celda.getData();   //se actualiza la variable peso con el peso adquirido
    //Funcion de AutoZero
    if (peso >= 0){ //si peso es positivo
      if (peso < pesoAutoZero){//si el peso es menor al limite del autozero
        peso = 0; //se pone a cero
      }
    }else{ //si el peso es negativo
      if (peso > (0-pesoAutoZero)){ //si el peso negativo es mayor al peso limite de autoZero
          peso = 0;
      }
    }

  } else { //si se trabaja en modo Entrada Serial (con indicador)
    if( Serial1.available() ){  //si hay datos disponibles
      String Str = Serial1.readString();  //se toma todo el string
      Str.trim(); //se eliminan espacios finales y saltos
      Serial.print(F("Recibido: "));  //se envia confirmacion de recepcion
      Serial.println(Str);
      String outputStr="";  //Se procesa el string para obtener el peso
      for (int i =0;i<Str.length(); i++){
        if ((Str[i] >= '0' && Str[i] <='9' ) || Str[i] == '.'){
          outputStr+=Str[i];
        }
      }
      peso=outputStr.toFloat(); //se actualiza la variable peso segun lo recibido
    }
  }
    peso = peso/precision;
    peso=round(peso);
    peso=peso*precision;

  switch (modo) {

    case 1:  {//Pantalla inicial
      if (Aceptar){
        if (textoStr.toInt()>=0){
          modo = 6;//pasa a modo de busqueda de producto
        }else  {
          Serial.println(F("Ingrese Prod ID"));
          lcd.setCursor(0,1);
          lcd.print(F("Ingrese Prod ID "));
        }
      }
      if (firstTime){
        lcd.noCursor();
        textoStr = "";
      }
      lcd.setCursor(0,0);
      lcd.print(completeLcdString(F("Inicio"),LONG_RENGLON));
      if (peso<cargaMax){
        String pesoStr= String(peso,pesoDecs);
        int subInd = (pesoStr.length() > 7 ) ? pesoStr.length()-7 : 0 ;
        pesoStr=pesoStr.substring(subInd);
        lcd.setCursor(LONG_RENGLON-pesoStr.length()-strUnit.length(),0);
        lcd.print(pesoStr);
        lcd.print(strUnit);
      }else{
        lcd.setCursor(LONG_RENGLON-7,0);
        lcd.print(F("**MAX**"));
      }
      lcd.setCursor(0, 1);
      String printed =F("Numero Prod: ");
      if( textoStr.toInt() <= 0){
        printed += F(" ? ");
      }else{
        printed += textoStr;
      }
      lcd.print(completeLcdString(printed,LONG_RENGLON));
      lcd.setCursor(0,2);
      lcd.print(completeLcdString("",LONG_RENGLON));
      lcd.setCursor(0,3);
      printed=fechaString(Now.day(),Now.month(),Now.year());
      printed+="  ";
      printed+=horaString(Now.hour(),Now.minute(),Now.second());
      lcd.print(printed);
      if (oneSec) {
        Serial.println(horaString(Now.hour(), Now.minute(), Now.second()));
        if (peso < cargaMax){
          Serial.print(peso,pesoDecs);
          Serial.println(strUnit);
        }else{
          Serial.println(F("**MAX**"));
        }
      }
      break;}

    case 2: {//Se pide numero de Lote
       if (Aceptar){
        numeroLote=textoStr.toInt();
        if (numeroLote>=0){
          modo = 7;//pasa a modo pide fecha elaboracion
        }else  {
          Serial.println(F("Ingrese Numero de Lote"));
          lcd.setCursor(0,1);
          String printed = F("Ingrese Lote: ");
          if (textoStr = ""){
            printed += "?";
          }else{
            printed += textoStr;
          }
          lcd.print(completeLcdString(printed,LONG_RENGLON));
        }
      }else if(Cancelar){
        numeroProd=-1;
        modo = 1;
      }
      if(firstTime){
        Serial.println(F("Ingrese numero de lote"));
      }
      if (oneDSec){
        lcd.setCursor(0,0);
        lcd.print(lcdPrintInManyRows(nombreProd,LONG_RENGLON, 1));
        lcd.setCursor(0,1);
        lcd.print(lcdPrintInManyRows(nombreProd,LONG_RENGLON, 2));
        lcd.setCursor(0, 2);
        lcd.print(completeLcdString("Ingrese Lote: "+textoStr,LONG_RENGLON));
        //lcd.print(textoStr);
        lcd.setCursor(15,2);
        lcd.cursor();
        lcd.blink();
      }
      break;}

    case 3: {  //pantalla con numero de Lote y Prod seleccionados(Principal)
      if (Aceptar){
          modo = 4;//pasa a modo de Impresion
      }else if(Cancelar){
        modo=1;
      }else{
        if (firstTime){
        lcd.noCursor();
        Serial.print(F("Peso: "));
        Serial.print(peso,pesoDecs);
        Serial.println(strUnit);
        Serial.print(F("Operador: "));
        Serial.println(nombreOperador);
        Serial.print(F("Producto: "));
        Serial.println(nombreProd);
        Serial.println(nickProd);
        Serial.print(F("RNPA: "));
        Serial.println(RNPA);
        Serial.println(fechaString(Now.day(),Now.month(), Now.year()  ));
        Serial.println(horaString(Now.hour(), Now.minute(), Now.second() ));
        Serial.print(F("Vencimiento: "));
        Serial.println(fechaString(Now.day(),Now.month(), Now.year()+1  ));
        Serial.print(F("Numero de Lote: "));
        Serial.print(clave);
        numeroLoteStr=String(numeroLote);
        if(numeroLoteStr.length()<6){//si el lote no tiene 6 digitos
          String comp="";
          for ( int n = 0 ; n < ( 6-numeroLoteStr.length());n++){// se completa con "0"
            comp += "0";
          }
          numeroLoteStr = comp + numeroLoteStr;
        }
        Serial.println(numeroLoteStr);
        Serial.print(F("Numero Ticket: "));
        Serial.println(ultimaSecuencia+1);
        Serial.print(F("ID: "));
        if (Now.day()<10){  //se crea el ID del dia
          opID="0";
        }else{
          opID="";
        }
        opID += Now.day();
        if ( Now.month()<10 ) opID += "0";
        opID += Now.month();
        opID += String(Now.year()-2000);
        Serial.println(opID);
        }
        lcd.setCursor(0,0);
        lcd.print(lcdPrintInManyRows("Prod: "+nombreProd,LONG_RENGLON,1));
        lcd.setCursor(0,1);
        lcd.print(lcdPrintInManyRows("Prod: "+nombreProd,LONG_RENGLON,2));
        lcd.setCursor(0,2);
        lcd.print(centerLcdString(clave+numeroLoteStr+" "+fechaElabStr,LONG_RENGLON));
        lcd.setCursor(0,3);
        String printed = F("Imprimir ? ");
        if (peso<cargaMax){
          printed += String(peso,pesoDecs)+strUnit;
        }else{
          printed += F("**MAX**");
        }
        lcd.print(completeLcdString(printed,LONG_RENGLON));

        if (oneSec) {
          Serial.print(horaString(Now.hour(),Now.minute(),Now.second()));
          Serial.print(" ");
          if (peso < cargaMax){
            Serial.print(peso);
            Serial.println(strUnit);
          }else{
            Serial.println(F("**MAX**"));
          }
        }
      }
      break;}

     case 4:{//Se realiza la impresion
       ultimaSecuencia++;
       EEPROM.put(addUltimaSecuencia,ultimaSecuencia);
       Serial.println("Imprimiendo...");
       lcd.setCursor(0,1);
       lcd.print(F("Imprimiendo...  "));
       //Impresion
       config_printer();
       printer.setFont(2);
       printer.addToBuffer(700,100,nombreProd);
       printer.setFont(3);
       String pesoOutput= String(peso) + " " + strUnit;
       printer.addToBuffer(600,300,pesoOutput);
       printer.setFont(2);
       printer.addToBuffer(500,100,"Elaboracion: ");
       printer.addToBuffer(500,500,fechaElabStr);
       printer.addToBuffer(400,100,"Vencimiento: ");
       printer.addToBuffer(400,500,fechaVencStr); //un anio desde la elaboracion
       String Lote= "Lote:  "+ String(clave);
       printer.addToBuffer(300,100,Lote);
       printer.addToBuffer(300,400,numeroLoteStr);
       printer.addToBuffer(200,100,"Fraccionado: ");
       printer.addToBuffer(200,470,fechaString(Now.day(),Now.month(),Now.year()));
       printer.addToBuffer(200,810,horaString(Now.hour(), Now.minute())+"     ");
       printer.printBuffer();
       printer.clearBuffer();
       delay(1500);
      modo = 3;
      break;}

    case 6: {//Busqueda de ProdID
      Serial.println("CASE 6");
      numeroProd = textoStr.toInt();
      textoStr="";
      Serial.println(numeroProd);
      renglon = getRenglonFromProd(numeroProd); //se toman todos los datos del producto
      if (renglon == "ERROR 401"){
        lcd.setCursor(0,1);
        lcd.print(F("Numero No Existe"));
        Serial.println(F("El Numero Ingresado no fue encontrado.\nIngrese numero de Producto Existente"));
        delay(1500);
        modo= 1; //se vuelve a la pantalla inicial
      }else if(renglon == "ERROR 402"){//si no se puede leer el archivo
        Serial.print(F("ERROR abriendo el archivo \"prods.txt\".\nCompruebe la SD Card"));
        lcd.setCursor(0,1);
        lcd.print(F("ERROR FILE prods"));
        delay(1500);
        modo=1;//se vuelve a la pantalla principal
      }else{
        nombreProd = getNombre(renglon);//se toma el nombre del producto
        nombreProd.trim();
        Serial.println(nombreProd);
        nickProd = getNick(renglon);//se toma el nombre corto
        nickProd.trim();
        Serial.println(nickProd);
        RNPA = getRNPA(renglon);//se toma el RNPA
        RNPA.trim();
        Serial.println(RNPA);
        modo=2;
      }
      break;}

      case 7:{  // Se pide la fecha de elaboracion
        if ( Aceptar ){
          int day =textoStr.substring(0,2).toInt();
          int month = textoStr.substring(2,4).toInt();
          int year = textoStr.substring(4,6).toInt() + 2000;
          if (day < 32 && month < 13 && year > 2020){
            fechaElabStr=fechaString(day, month,year);
            fechaVencStr=fechaString(day,month,year+1);

            modo = 3;
          }else{
            textoStr  = "******";
          }
        }else if ( Cancelar ){
          modo = 1;
        }else{
          textoStr=textoStr.substring(textoStr.length()-6);
          lcd.setCursor(0,0);
          lcd.print(F(" Ingrese Fecha Elab "));
          lcd.setCursor(0,1);
          lcd.print(F("Formato DD/MM/YY"));
          lcd.setCursor(0,2);
          lcd.print(F("Ingreso:               "));
          lcd.setCursor(9,2);
          lcd.print(textoStr.substring(0,2));
          lcd.print("/");
          lcd.print(textoStr.substring(2,4));
          lcd.print("/");
          lcd.print(textoStr.substring(4,6));
          if (firstTime){
            textoStr="      ";
            Serial.println(F("Ingrese Fecha de Elaboracion en formato DDMMYY.\nLuego 'Aceptar'"));
           }
        }
        break;
      }


///*Modos para la Funcionalidad principal*///
    case 10: { //Se ingresa el Prod ID

       ////Falta la visualizacion de datos en el display

      break;
      }


//*Modos para la Configuracion*//
    case 100: //Pantalla de Configuracion General
       if (firstTime){
        textoStr="";
        indice_conf=1;
        for (int i = 1; i <= opcionesConf_len; i++){
          Serial.print(i);
          Serial.print("=");
          Serial.println(opcionesConf[i-1]);
        }
        Serial.println(F("Final del Menu Configuracion\nIngrese una opcion y luego 'Aceptar'"));
      }else{
        if (Aceptar){ //si se recibe aceptar
          indice_confAnt = 0;
          modo=100+indice_conf;
        }else if(Cancelar){
          indice_confAnt = 0;
          modo=1;
        }else if(Asterisco){//si se aprieta '*'
          textoStr="";
          if (indice_conf < opcionesConf_len){ //si no se llego a la ultima opcion
            indice_conf++; //se avanza una opcion
          }else {//si se llega a la ultima opcion
            indice_conf = 1;  //se vuelve a la primera
          }
        }else if(Hash){ //si se aprieta '#'
          textoStr="";
          if (indice_conf > 1){ //y no se encuentra en la  primer opcion
            indice_conf--;  //se va una opcion hacia atras
          }else{//si se llega a la opcion 1 se pasa a la ultima
            indice_conf = opcionesConf_len;
          }
          //si el dato recibido por puerto serie esta entre 0 y la opcion mayor
        }else if(  textoStr.toInt() >= 1 && textoStr.toInt() <= opcionesConf_len){
          indice_conf = textoStr.toInt();//
          textoStr = "";
        }else{  //si se ingresa una opcion equivocada se selecciona la primera
              textoStr = "";
        }
      }
      if (indice_conf != indice_confAnt){//si el indice seleccionado es diferente al anterior
            indice_confAnt=indice_conf;
            Serial.print(F("Opcion seleccionada: "));
            Serial.println(opcionesConf[indice_conf-1]);
          }
      lcd.setCursor(0,0);
      lcd.print(F("Configuracion :"));
      lcdPrintSpaces(LONG_RENGLON - 15);
      lcd.setCursor(0,1);
      lcd.print(opConfAnt(indice_conf));
      lcdPrintSpaces(LONG_RENGLON - opConfAnt(indice_conf).length());
      lcd.setCursor(0,2);
      lcd.write(126);
      lcd.print(opcionesConf[indice_conf-1]);
      lcdPrintSpaces(LONG_RENGLON - opcionesConf[indice_conf-1].length()-1);
      lcd.setCursor(0,3);
      lcd.print(opConfPost(indice_conf));
      lcdPrintSpaces(LONG_RENGLON - opConfPost(indice_conf).length());
      break;

    case 101: //Cambio de turno
      if (Aceptar){
        Serial.println("Aceptar 101");
        modo = 201;
      }else if(Cancelar){
        modo = 1;
      }else {
        lcd.setCursor(0,0);
        lcd.print(completeLcdString(F("Ingrese Turno: "),LONG_RENGLON));
        lcd.setCursor(0,1);
        lcd.print(centerLcdString(textoStr,LONG_RENGLON));
        lcd.setCursor(0,2);
        lcd.print(centerLcdString(F("A:ok    C:Cancel"),LONG_RENGLON));
        lcd.setCursor(0,3);
        lcd.print(completeLcdString(" ",LONG_RENGLON));
        if (firstTime){
          textoStr="";
          Serial.println(F("Ingrese numero de Turno nuevo.\n Luego 'Aceptar'"));
         }
      }
      break;

    case 102: //Cambio de Operador
      if ( Aceptar ){
        modo = 202;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(completeLcdString(F("Ingrese Operador: "),LONG_RENGLON));
        lcd.setCursor(0,1);
        lcd.print(centerLcdString(textoStr,LONG_RENGLON ));
        lcd.setCursor(0,2);
        lcd.print(centerLcdString(F("A:ok   C:Cancel"),LONG_RENGLON));
        lcd.setCursor(0,3);
        lcd.print(completeLcdString(" ",LONG_RENGLON));
        if (firstTime){
          textoStr="";
          Serial.println(F("Ingrese numero de Operador nuevo.\n Luego 'Aceptar'"));
         }
      }
      break;

    case 103: //Cambio de Clave alFANUMERICA
      if ( Aceptar ){
        modo = 203;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(completeLcdString(F("Ingrese CLAVE:"),LONG_RENGLON));
        lcd.setCursor(0,1);
        lcd.print(centerLcdString(textoStr,LONG_RENGLON));
        lcd.setCursor(0,2);
        lcd.print(centerLcdString(F("A:ok    C:Cancel"),LONG_RENGLON));
        lcd.setCursor(0,3);
        lcd.print(completeLcdString(" ",LONG_RENGLON));
        if (firstTime){
          textoStr="";
          Serial.println(F("Ingrese nueva CLAVE .\n Luego 'Aceptar'"));
         }
      }
      break;

    case 104: //Calibracion
      if ( Aceptar ){
        modo = 51;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(F(" Inicio Calibracion "));
        lcd.setCursor(0,1);
        lcd.print(F(" Descargue Balanza  "));
        lcd.setCursor(0,2);
        lcd.print(completeLcdString(F("Luego oprima A"),LONG_RENGLON));
        lcd.setCursor(0,3);
        lcd.print(completeLcdString(" ",LONG_RENGLON));
        if (firstTime){
          textoStr="";
          Serial.println(F("Descargue la Balanza para la Calibracion.\n Luego 'Aceptar'"));
         }
      }
      break;

    case 105: //Precision
    if ( Aceptar ){
      modo = 205;
    }else if ( Cancelar ){
      modo = 1;
    }else if (Asterisco){
      textoStr+=".";
    }else    {
      lcd.setCursor(0,0);
      lcd.print(F(" Nueva precision "));
      lcd.setCursor(0,1);
      lcd.print(textoStr);  //muestra el dato ingresado
      for(int n=0; n <13-textoStr.length(); n++){ //completa con espacios hasta el espacio 13
        lcd.print(F(" "));
      }
     // lcd.setCursor(13,1);
      lcd.print(F("A:ok"));
      if (firstTime){
        textoStr="";
        Serial.println(F("Ingrese cual sera la cantidad minima a sensar.\nLuego Ingrese 'Aceptar'"));
       }
    }
      break;

    case 106: //Carga Maxima
      if ( Aceptar ){
        modo = 206;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(F("  Carga Maxima  "));
        lcd.setCursor(0,1);
        lcd.print(F("Actual:"));
        lcd.print(cargaMax);
        lcd.print(strUnit);
        lcd.setCursor(0,2);
        lcd.print(F("Nueva: "));
        lcd.setCursor(13,1);
        lcd.print(textoStr);
        if (firstTime){
          textoStr="";
          Serial.print(F("Carga Maxima actual: "));
          Serial.print(cargaMax);
          Serial.println(strUnit);
          Serial.println(F("Ingrese el nuevo valor de Carga Maxima\n Luego 'Aceptar'"));
         }
      }


        break;

    case 107: //Modo de lectura
      if ( Aceptar || textoStr == "SI"){
        modo = 207;
      }else if ( Cancelar || textoStr == "NO"){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        if (dSecCount/5==1){ //destella cada medio segundo
          lcd.print(F("  Cambio Modo  "));
        }else{
          lcd.print(F("               "));
        }
        lcd.setCursor(0,1);
        lcd.print(F("Actual:"));
        if (modoLectura == MODO_CELDA){
            lcd.print(F(" CELDA   "));
        }else{
            lcd.print(F("INDICADOR"));
        }
        if (firstTime){
          textoStr="";
          if (modoLectura == MODO_CELDA){
            Serial.println(F("EL modo actual es CELDA. \nSi desea cambiar a modo INDICADOR ingrese 'SI', o 'NO' para salir "));
          }else {
            Serial.println(F("EL modo actual es INDICADOR. \nSi desea cambiar a modo CELDA ingrese 'SI', o 'NO' para salir "));
          }
        }
      }
      break;

    case 108: //configuracion de las unidades
      if ( Aceptar ){
        modo = 208;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(F("Seleccion Unidad"));
        lcd.setCursor(0,1);
        lcd.print(F("Ingrese: "));
        lcd.print(textoStr);
        if (firstTime){
          textoStr="";
          Serial.println(F("Ingrese uno o dos caracteres para las Unidades.\n Luego 'Aceptar'"));
         }
      }
        break;

    case 109: //Configuracion de la Hora
      if ( Aceptar ){
        modo = 209;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        textoStr=textoStr.substring(textoStr.length()-4);
        lcd.setCursor(0,0);
        lcd.print(F("Ingrese HH:MM "));
        lcd.setCursor(0,1);
        lcd.print(F("Ingreso: "));
        lcd.print(textoStr.substring(0,2));
        lcd.print(":");
        lcd.print(textoStr.substring(2,4));
        if (firstTime){
          textoStr="0000";
          Serial.println(F("Ingrese Hora a actualizar en formato HHMM.\n Luego 'Aceptar'"));
         }
      }
      break;

    case 110: //Configuracion de Fecha
            if ( Aceptar ){
              modo = 210;
            }else if ( Cancelar ){
              modo = 1;
            }else{
              textoStr=textoStr.substring(textoStr.length()-6);
              lcd.setCursor(0,0);
              lcd.print(F("Ingrese DD/MM/AA "));
              lcd.setCursor(0,1);
              lcd.print(F("Ingreso:"));
              lcd.print(textoStr.substring(0,2));
              lcd.print("/");
              lcd.print(textoStr.substring(2,4));
              lcd.print("/");
              lcd.print(textoStr.substring(4,6));
              if (firstTime){
                textoStr="000000";
                Serial.println(F("Ingrese Fecha a actualizar en formato DDMMYY.\nLuego 'Aceptar'"));
               }
            }
            break;

     case 111: {      //Configuracion del Baudrate de la entrada Serial(Indicador)
      if ( Aceptar ){
        Serial.println("OK!!!");
        modo = 211;
      }else if ( Cancelar ){
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(F("Ingrese Baudrate"));
        lcd.setCursor(0,1);
        lcd.print(F("Ingreso: "));
        lcd.print(textoStr);
        lcdPrintSpaces(LONG_RENGLON-9-textoStr.length());
        if (firstTime){
          textoStr="0";
          Serial.print(F("Baudrate Actual: "));
          Serial.println(baudrateS1);
          Serial.println(F("Ingrese el Baudrate del Indicador a usar.\nLuego 'Aceptar'"));
         }
      }
      break;
      }


     case 112: {// se inicia la Tara de la balanza
        if ( Aceptar ){
          if (modoLectura == MODO_CELDA){ //si se trabaja en modo celda
            Serial.println(F("Se inicia Tara de la balanza"));
            modo = 50;  //se pasa a la tara
          }else{//si se usa el modo indicador, no se usa esta funcion
            Serial.println  (F("Realice la tara de la balanza desde el Indicador."));
            lcd.setCursor(0,1);
            lcd.print(F("TARAR  INDICADOR"));
            modo = 1;
          }
        }else if ( Cancelar ){
          modo = 1;
        }else{
          lcd.setCursor(0,0);
          lcd.print(F(" Inicio TARA  "));
          lcd.setCursor(0,1);
          lcd.print(F("Descargue    "));
          lcd.setCursor(13,1);
          lcd.print(F("A:ok"));
        if (firstTime){
          textoStr="";
          Serial.println(F("Descargue la Balanza para la Tara.\nLuego 'Aceptar'"));
          }
        }
        break;
        }


    case 113: // Direccion I2C LCD
    if ( Aceptar ){
      modo = 213;
    }else if ( Cancelar ){
      modo = 1;
    }else{
      lcd.setCursor(0,0);
      lcd.print(F(" Direccion I2C LCD "));
      lcd.setCursor(0,1);
      lcd.print(textoStr);  //muestra el dato ingresado
      for(int n=0; n <13-textoStr.length(); n++){ //completa con espacios hasta el espacio 13
        lcd.print(F(" "));
      }
      lcd.print(F("A:ok"));
      if (firstTime){
        textoStr="";
        Serial.print(F("Actual Direccion I2c Display: "));
        Serial.println(LcdI2cAddress);
        Serial.println(F("Ingrese la direccion I2C del display LCD.\nLuego Ingrese 'Aceptar'"));
       }
    }
      break;

      case 114: // Direccion I2C DS3231
    if ( Aceptar ){
      modo = 214;
    }else if ( Cancelar ){
      modo = 1;
    }else{
      lcd.setCursor(0,0);
      lcd.print(F(" Direccion I2C RTC "));
      lcd.setCursor(0,1);
      lcd.print(textoStr);  //muestra el dato ingresado
      for(int n=0; n <13-textoStr.length(); n++){ //completa con espacios hasta el espacio 13
        lcd.print(F(" "));
      }
      lcd.print(F("A:ok"));
      if (firstTime){
        textoStr="";
        Serial.print(F("Actual Direccion Reloj RTC: "));
        Serial.println(RtcI2cAddress);
        Serial.println(F("Ingrese la direccion I2C del Reloj DS3231.\nLuego Ingrese 'Aceptar'"));
       }
    }
      break;

      case 115: // Brillo Display LCD
    if ( Aceptar ){
      if (textoStr.toInt() < 256){
        modo = 215;
      }else{
        textoStr = "";
      }
    }else if ( Cancelar ){
      modo = 1;
    }else{
      lcd.clear();
      lcd.print(F(" Brillo LCD "));
      lcd.setCursor(0,1);
      lcd.print(F("Actual: "));
      lcd.print(lcd_led_pow);
      lcd.print(F("/255"));
      lcd.setCursor(0,2);
      lcd.print(F("Nuevo: "));
      lcd.print(textoStr);  //muestra el dato ingresado
      lcd.setCursor(0,3);
      lcd.print(F("A:ok"));
      if (firstTime){
        textoStr="";
        Serial.print(F("Actual valor del Brillo lCD: "));
        Serial.println(lcd_led_pow);
        Serial.println(F("Ingrese valor nuevo Brillo (0-255).\nLuego Ingrese 'Aceptar'"));
       }
    }
      break;

      case 116: {  //Ajuste del autoZero
        if ( Aceptar ){
          modo = 216;
        }else if ( Cancelar ){
          modo = 1;
        }else{
          if (firstTime){
            lcd.clear();
            textoStr="";
            Serial.print(F("Actual porcentaje de AutoZero: "));
            Serial.println(autoZeroLimit);
            Serial.println(F("Ingrese el nuevo porcentaje para AutoZero.\nLuego Ingrese 'Aceptar'"));
           }
          lcd.setCursor(0,0);
          lcd.print(F(" Ajuste AutoZero "));
          lcd.setCursor(0,1);
          lcd.print(centerLcdString(textoStr,20));  //muestra el dato ingresado
          lcd.setCursor(0,2);
          lcd.print(F("A:ok"));

        }
        break;
      }



    case 50: //(ex 101) se tara la balanza y se guardan el offset
      Serial.print(F("Nuevo Tare OffSet:"));
      oldTareOffset = tareOffset;
      celda.tare();
      tareOffset = celda.getTareOffset();
      EEPROM.put(addOffset, tareOffset);
      Serial.println(tareOffset);
      lcd.setCursor(0,1);
      lcd.print(F("Nuevo OS:"));
      lcd.print(tareOffset);
      delay(2000);
      modo=1;
      break;

    case 51: //se tara la balanza y se guardan el offset y se pasa a calibracion
      celda.tare();
      tareOffset = celda.getTareOffset();
      celda.setCalFactor(1.0);
      modo=52;
      break;

    case 52: //Se pide carga patron
      if ( Aceptar ){
        pesoIndicado = textoStr.toFloat();
        if (pesoIndicado > 0){  //si se introdujo un peso Patron
          modo = 53;
        }else{  //si no se introdujo un peso patron Se sale de la configuracion
          lcd.setCursor(0,1);
          lcd.print(F("Peso incorrecto"));
          Serial.println(F("Peso ingresado incorrecto.Saliendo"));
          celda.setTareOffset(EEPROM.read(addOffset)); //se anula la tara hecha
          delay(1500);
          modo=1;
        }
      }else if ( Cancelar ){ //si se cancela la calibracion
        celda.setTareOffset(EEPROM.read(addOffset));//se anula la tara hecha
        modo = 1;
      }else{
        lcd.setCursor(0,0);
        lcd.print(completeLcdString(F("Calib. Ingrese"),LONG_RENGLON));
        lcd.setCursor(0,1);
        lcd.print(F("Peso: "));
        lcd.print(textoStr);
        lcd.print(strUnit);
        lcd.print(completeLcdString(" ",LONG_RENGLON-6));
        lcd.setCursor(0,2);
        lcd.print(centerLcdString("Lectura:",LONG_RENGLON));
        lcd.setCursor(0,3);
        lcd.print(centerLcdString(String(peso,2),LONG_RENGLON));
        //lcd.print(strUnit);
        if (firstTime){
          pesoIndicado=0;
          textoStr="";
          textoStr="";
          Serial.println(F("Ingrese Peso Patron.\n Luego 'Aceptar'"));
         }
      }
      break;

    case 53: //Se guarda nuevo coeficiente de Calibracion
      EEPROM.put(addOffset, tareOffset);// se actualiza la tara en la eeprom
      Serial.print(F("Guardado nuevo OffSet: "));
      Serial.println(tareOffset);
      calValue = celda.getNewCalibration(pesoIndicado);
      celda.setCalFactor(calValue);
      Serial.print("Guardado nuevo factor de calibracion:");
      Serial.println(calValue);
      EEPROM.put(addCalVal,calValue);
      lcd.setCursor(0,0);
      lcd.print(" Calibracion OK ");
      lcd.setCursor(0,1);
      lcd.print(calValue);
      lcd.print(F(" Cal Value     "));
      delay(2000);
      modo = 1;
      break;

    case 54:  //se vuelve a la tara anterior si es que habia una guardada
      if ( oldTareOffset > 0){
        long oldOldOffset = oldTareOffset;
        oldTareOffset= tareOffset;
        tareOffset = oldOldOffset;
        celda.setTareOffset(tareOffset);
        EEPROM.put(addOffset, tareOffset);
      }
      modo = prevModo;
      break;

    case 201: //Actualizacion de nuevo Turno -ant=modo 101
      numeroTurno = textoStr.toInt();
      nombreTurno = getNombreTurno(numeroTurno);
      EEPROM.put(add_numeroTurno,numeroTurno);
      Serial.print(F("Nuevo Turno: "));
      Serial.println(nombreTurno);
      lcd.setCursor(0,1);
      lcd.print(F("Nuevo: "));
      lcd.print(nombreTurno);
      delay(2000);
      modo = 1;
      break;

    case 202: //Actualizacion de nombre y numero de operador -ant=modo 102
      numeroOperador=textoStr.toInt();
      nombreOperador = getNombreOperador(numeroOperador);
      EEPROM.put(add_numeroOperador, numeroOperador);
      Serial.print(F("Nuevo Operador: "));
      Serial.println(nombreOperador);
      modo = 1;
      break;

    case 203: //Actualizacion de clave ALFANUMERICA
      clave=textoStr;
      for ( int n=0; n < clave.length();n++){
        Serial.print(n);
        Serial.println(clave.charAt(n));
        EEPROM.put(add_clave + n, clave.charAt(n));
      }
      modo = 1;
      break;

    case 205: //Actualizacion de la precision
      if(textoStr.toFloat() > 0.01){
        precision=textoStr.toFloat();
        EEPROM.put(addPrecision,precision);
      }else{
        precision=precDefault;
        EEPROM.put(addPrecision, precision);
      }
      lcd.setCursor(0,1);
      lcd.print(F("Precision: "));
      lcd.print(String(precision));
      lcd.print(strUnit);
      Serial.print(F("La nueva precision establecida es: "));
      Serial.print(precision);
      Serial.print(strUnit);
      delay(2000);
      modo = 1;
      break;

    case 206: //actualizacion de la carga maxima de la balanza
      cargaMax = textoStr.toFloat();
      EEPROM.put(addCargaMax, cargaMax);
      lcd.setCursor(0,1);
      lcd.print(F("Carga Max:"));
      lcd.print(textoStr);
      lcd.print(strUnit);
      Serial.print(F("La nueva carga Maxima configurada es de: "));
      Serial.print(cargaMax);
      Serial.println(strUnit);
      delay(2000);
      modo=1;
      break;

    case 207://Cambio de modo de Lectura )Indicador o celda
      if ( modoLectura == MODO_CELDA){
        EEPROM.put(  addModoLectura,char( MODO_SERIE));
        lcd.setCursor(0,1);
        lcd.print(F("Reinicie -NUEVO"));
        lcd.setCursor(1,0);
        lcd.print("MODO INDICADOR ");
        Serial.println(F("Reinicie el dispositivo para iniciar con nuevo modo INDICADOR"));
      }else {
        EEPROM.put(  addModoLectura, char(MODO_CELDA));
        lcd.setCursor(0,1);
        lcd.print(F("Reinicie NUEVO"));
        lcd.setCursor(1,0);
        lcd.print("MODO   CELDA  ");
        Serial.println(F("Reinicie el dispositivo para iniciar con nuevo modo CELDA"));
      }
      while(1);
      break;

    case 208: //Actualizacion de las unidades de peso
      if (textoStr.toInt() >= 0){
        switch (textoStr.toInt()){
          case 0:
            textoStr="KG";
            break;

          case 1:
            textoStr="g";
            break;

          case 2:
            textoStr="Oz";
            break;

          case 3:
            textoStr="lb";
            break;
        }
      }
      strUnit=textoStr.substring(0,2);
      EEPROM.put(addStrUnit, strUnit.charAt(0));
      EEPROM.put(addStrUnit + 1, strUnit.charAt(1));
      Serial.print(F("Unidades cambiadas a: "));
      Serial.println(strUnit);
      lcd.setCursor(0,1);
      lcd.print("  Unidades: ");
      lcd.print(strUnit);
      lcd.print(F("  "));
      delay(2000);
      modo = 1;
      break;

    case 209: {//Se actualiza la Hora en el RTC
      int hora = textoStr.substring(0,2).toInt();
      int mins = textoStr.substring(2,4).toInt();
      Serial.print(F("Se estableció la hora a  "));
      lcd.setCursor(0,1);
      lcd.print(F("Nueva Hora "));
      if ( hora < 24 && hora >= 0){
        setterClock.setHour(hora);
        lcd.print(hora);
        Serial.print(hora);
      }
      lcd.print(":");
      if ( mins < 60 && mins >= 0){
        lcd.print(mins);
        Serial.print(":");
        Serial.println(mins);
        setterClock.setMinute(mins);
      }
      textoStr="";
      delay(2000);
      modo = 1;
      break;
      }

    case 210: {// se actualiza la fecha en el RTC
      if (textoStr.length() == 6){
        int dia = textoStr.substring(0,2).toInt();
        int mes = textoStr.substring(2,4).toInt();
        int anio = textoStr.substring(4,6).toInt();

        Serial.print(F("Se estableció la Fecha: "));
        lcd.setCursor(0,0);
        lcd.print(F("Nueva Fecha:    "));
        lcd.setCursor(0,1);
        lcd.print(F("   "));
        if ( dia < 32 && dia >= 0){
          setterClock.setDate(dia);
          if ( dia <10){
            lcd.print("0");
            Serial.print("0");
          }
          lcd.print(String(dia));
          Serial.print(dia);
        }else{
          lcd.print(F("EE") );
          Serial.print(F("EE") );
        }
        Serial.print(F("/"));
        lcd.print(F("/"));
        if ( mes < 13 && mes > 0){
          lcd.print(String(mes));
          Serial.print( String(mes ));
          setterClock.setMonth(mes);
        }else{
          lcd.print(F("EE") );
          Serial.print(F("EE") );
        }
        Serial.print(F("/20"));
        lcd.print(F("/20"));
        if ( anio >= 22){
          lcd.print(String(anio));
          Serial.println(anio );
          setterClock.setYear(anio);
        }else{
          lcd.print(F("EE"));
          Serial.print(F("EE"));
        }
        lcd.print(F("      "));
      }else{
        Serial.println(F("El dato ingresado es incorrecto.Saliendo..."));
      }
      textoStr="";
      delay(2000);
      modo = 1;
      break;
      }

  case 211: //actualizacion del baudrateS1
    Serial.println("211!");
    if (textoStr.toInt() < 5){ //si el dato ingresado es muy chico
      Serial.println("!1");
      switch (textoStr.toInt()){//usamos la configuracion por tabla
        case 0:
          baudrateS1=1200;
          break;

        case 1:
          baudrateS1=2400;
          break;

        case 2:
          baudrateS1=4800;
          break;

        case 3:
          baudrateS1=9600;
          break;
      }
    }else{  Serial.println("2");  //si se ingresa el baudrate completo se usa ese dato
      baudrateS1 = textoStr.toInt();
    }
    Serial1.begin(baudrateS1);
    EEPROM.put(addBaudrateS1,baudrateS1); //se actualiza la EEPROM
    Serial.print(F("El nuevo  baudrate es: "));
    Serial.println(baudrateS1);
    lcd.setCursor(0,0);
    lcd.print(F("Nuevo Baudrate:"));
    lcd.setCursor(0,1);
    lcd.print(F("   "));
    lcd.print(String(baudrateS1));
    lcd.print(F("         "));
    delay(2000);
    modo = 1;
    break;

    case 213: //Cambio de la direccion I2C del LCD
      if(textoStr.toInt() > 0){
        LcdI2cAddress=textoStr.toInt();
        EEPROM.put(addLcdI2cAddress, LcdI2cAddress);
      }else{
        Serial.println(F("DIreccion Default"));
        LcdI2cAddress=0x27;
        EEPROM.put(addLcdI2cAddress, LcdI2cAddress);
      }
      lcd.setCursor(0,1);
      lcd.print(F("LCD I2C: "));
      lcd.println(LcdI2cAddress);
      Serial.print(F("La direccion LCD establecida es: "));
      Serial.print(LcdI2cAddress);
      delay(2000);
      modo = 1;
      break;

    case 214: //Cambio de la direccion I2C del LCD
      if(textoStr.toInt() > 0){
        RtcI2cAddress=textoStr.toInt();
        EEPROM.put(addRtcI2cAddress, RtcI2cAddress);
      }else{
        RtcI2cAddress=0x18;
        EEPROM.put(addRtcI2cAddress, RtcI2cAddress);
      }
      lcd.setCursor(0,1);
      lcd.print(F("RTC I2C: "));
      lcd.println(RtcI2cAddress);
      Serial.print(F("La direccion Rtc establecida es: "));
      Serial.println(RtcI2cAddress);
      delay(2000);
      modo = 1;
      break;

    case 215:
      lcd_led_pow = textoStr.toInt();
      EEPROM.put(add_lcd_led_pow,lcd_led_pow);
      SoftPWMSet(lcd_led,lcd_led_pow);
      Serial.print(F("Nuevo brillos LCD: "));
      Serial.print(lcd_led_pow);
      Serial.println("/256");
      lcd.clear();
      lcd.print(centerLcdString(F("Nuevo Brillo" ),LONG_RENGLON));
      lcd.setCursor(0,2);
      lcd.print(centerLcdString(String(lcd_led_pow),LONG_RENGLON));
      delay(2500);
      modo = 1;
      break;

    case 216: { //se modifica el autoZero
      if ( textoStr.toFloat() < autoZeroMaxLimit){
        autoZeroLimit=textoStr.toFloat();
        pesoAutoZero= cargaMax/100*autoZeroLimit;
        EEPROM.put(addAutoZeroLimit,autoZeroLimit);
        Serial.print(F("Nuevo porcentaje de AutoZero: "));
        Serial.println(autoZeroLimit);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print(centerLcdString(F("Nuevo AutoZero"),LONG_RENGLON));
        lcd.setCursor(0,2);
        String printed = String(autoZeroLimit,2) + "%";
        lcd.print(centerLcdString(printed,LONG_RENGLON));
        lcd.setCursor(0,3);
        pesoAutoZero= cargaMax/100*autoZeroLimit;
        printed = String(pesoAutoZero,2) + strUnit;
        lcd.print(centerLcdString(printed,LONG_RENGLON));
        delay(3000);
        modo = 1;
      }else{
        textoStr="";
        Serial.println(F("Valor Ingresado incorrecto"));
        lcd.clear();
        lcd.setCursor(1,0);
        lcd.print(centerLcdString(F("Valor Ingresado Incorrecto"),LONG_RENGLON));
        delay(2000);
        modo = 115;
      }

      break;}

    default:
      Serial.println("Error de modo");
      delay(1000);

      modo=1;
      break;


  }
  if (oneSec) { //rutina que se ejecuta cada 1 segundo
    oneSec = 0;
    Now=Clock.now();//actualiza la hora cada 1 segundo
  //  Serial.println(modo); //para debug
  }
  if (oneDSec) { //rutina que se ejecuta cada 100mS
    oneDSec = 0;
  }
  Asterisco = 0;
  Hash = 0;
  Aceptar = 0;
  Cancelar = 0;
}

void keypadEvent(KeypadEvent tecla) {
  switch (Teclado.getState()) {
    case HOLD:
      if (tecla == '*') { //si se mantiene ´*´
        lcd_led_pow = lcd_led_pow - 10; ///disminuye la luminosidad del display
        SoftPWMSet(lcd_led, lcd_led_pow);
      } else if (tecla == '#') { // si se mantiene ´#´
        lcd_led_pow = lcd_led_pow + 10; //aumenta la luminosidad del display
        SoftPWMSet(lcd_led, lcd_led_pow);
        //si se mantiene presionado un numero del 1 al 4 en modo manual
      } else if (tecla == 'C') { // si se mantiene presionado C
        if (modo ==1) modo = 100; //Entra en modo Configuracion

      }else if(tecla== 'B'){//si se mantiene 'B'

      }
      hold = 1;
      break;
    case RELEASED:
      hold = 0;
      break;
    case PRESSED:
      if (tecla == '*' ){  //si se presiona *
        if (modo < 99 ){
          prevModo = modo;
          modo=50 ;
        }
        Asterisco = true;
        if (textoStr.length()==0){//si el string tiene algun valor
          textoStr="0";
        }else {
          if (modo != 105){
            textoStr.remove(textoStr.length()-1);//se borra el ultimo
          }
        }
      } else if (tecla == '#'){
        if (modo < 99){
          prevModo = modo;
          modo = 54;
        }
        Hash = true;
      }  else if (tecla == 'C') { //si se presiona C
          Cancelar=1; //se activa Cancelar
      } else if (tecla == 'A' ) {//si se presiona A
          Aceptar=1;  //Se activa Cancelar
      }else if (tecla == 'B'){

      } else {//si se presiona un numero
        if (tecla >= '0' || tecla <= '9') textoStr += tecla;///se agrega al String

      }
      break;
  }

}

void countDSecCount() { //funcion que cuenta cada 10ms desde 1 a 100 y activa la variabel oneSec
  if (modoLectura == MODO_CELDA) celda.update();//se actualiza la medicion cada 10 ms
  if (dSecCount%10 == 0){
    oneDSec = 1;
  }
  dSecCount++;
  if (dSecCount % 100 == 0) {
    oneSec = 1;
  }
}

void serialEvent3(){
  String inputStr = Serial3.readString();
  inputStr.trim();
  inputStr.toUpperCase();
  Serial.print(F("Recibido BT: \""));
  Serial.print(inputStr);
  Serial.println("\"");
  if (inputStr == "OKS3"){
    Aceptar = 1;
  }else if(inputStr == "CANCELS3"){
    Cancelar = 1;
  }else{
    textoStr = inputStr;
  }
}

void serialEvent() {  //Codigo que se ejecuta al recibir algo por Puerto Serie (PC)
  String inputStr = Serial.readString();
  inputStr.trim();
  inputStr.toUpperCase() ;
  Serial.print(F("Recibido: \""));
  Serial.print(inputStr);
  Serial.println("\"");
  if (inputStr == "CONF") {
   modo=100;
  }else if(inputStr=="TARA"){
    modo=112;
  }else if(inputStr== "CANCEL"){
    Cancelar=1;
  }else if(inputStr == "OK" || inputStr == "ACEPTAR"){
    Aceptar=1;
  }else if(inputStr == "*") {
    Asterisco = true;
  }else if( inputStr == "#"){
    Hash = true;
  }  else{
    textoStr=inputStr;
  }
}

String getLineFromFile(int linea, String file){ //devuelve la 'linea' del 'file', sin el numero//Toda la logica de lectura esta aca
//  Formato de cada linea del archivo:
//"numero" "Nombre" "Nick" "RNPA"
//  00,nombre apellido \n
   File myfile = SD.open(file);//se crea la variable file y se le asigan el archivo de la SD
     String lineaStr; //se crea la variable String donde se almacena el resultado
   if (myfile){ //Si el archivo se abrio correctamente
    char inputF;  //Variable para almacenar el caracter leido del archivo
    int l =0; //variable para colocar el numero de linea
    int comCount=0;
    while(myfile.available()){  //Se lee todo el archivo caracter por caracter
      inputF=myfile.read(); //se asigna un nuevo caracter del archivo a la variable inputF
      if ( inputF == '\n' ){  //si el ultimo caracter es un salto de linea
        l=lineaStr.substring(0,lineaStr.indexOf(',')).toInt();  //se lee el numero de linea
        if ( l == linea){ //si concuerda con el numero pedido se puede terminar
          myfile.close(); //se cierra el archivo
          return lineaStr.substring(lineaStr.indexOf(',')+1); //se termina la ejecucion de la funcion y se retorna el nombre
        }else{    //si la linea no es la pedida
          comCount=0; //se resetea el
          lineaStr="";  //se elimina el contenido del resultado
        }
      }else{  //si el caracter no es fin de linea
        if (inputF == '"') {//si el caracter recibido es '"'
          comCount ++;//aumenta el contador de comillas
          if (comCount == 2){//si es la segunda comilla
            comCount=0;//se resetea el contador
            lineaStr+=','; //se agrega una coma al string resultado
          }
        }else {//si no es una comilla
          lineaStr+=inputF; //se agrega al string resultado

        }
      }
    }
    lineaStr= "ERROR 401"; //Numero de linea inexistente//si se llego al final del archivo
   }else{//si no se puede abrir el archivo
    lineaStr="ERROR 402";}//Error al abrir el archivo
  myfile.close();
  return lineaStr;

}

String getNombreOperador(int numero ){ //Devuelve el nombre del operador en la linea 'numero' del file 'operador.txt'
  return getLineFromFile(numero , "operador.txt");
}

String getNombreTurno(int numero){ //devuelve el nombre en la linea 'numero' del archivo ´turno.txt'
  return getLineFromFile(numero, "turno.txt");
}


String getRenglonFromProd(int prodID){
  return getLineFromFile(prodID, "prods.txt"); //Se saca el renglon numero prodID  del archivo prods.txt
}

String getCampoFromLine(String linea, int campo){
  String resultado = "";
  for (int n = 0; n<campo; n++){
    //Serial.println(n);
    //Serial.println(linea);
    resultado=linea.substring(0, linea.indexOf(','));
    //Serial.println(resultado);
    linea.remove(0,resultado.length()+1);

  }
  return resultado;
}

String getNombre(String linea){
  return getCampoFromLine(linea,1);
}

String getNick ( String linea){
    return getCampoFromLine(linea,2);
}

String getRNPA(String linea){
    return getCampoFromLine(linea,3);
}


String fechaString(int dia, int mes, int anio){
  String result="";
  if (dia<10) result+="0";
  result+=dia;
  result+="/";
  if (mes < 10)result += "0";
  result += mes;
  result+= "/";
  result += anio;
  return result;
}

String horaString(int hora, int minutos, int segundos){
  String result="";
  if (hora < 10)result += "0";
  result += hora;
  result += ":";
  if (minutos < 10)result += "0";
  result += minutos;
  result += ":";
  if (segundos <10)result += "0";
  result += segundos;
  return result;
}
String horaString(int hora, int minutos){
  String result="";
  if (hora < 10)result += "0";
  result += hora;
  result += ":";
  if (minutos < 10)result += "0";
  result += minutos;

  return result;
}

void lcdPrintSpaces(int spaces){
  for (int n = 0; n < spaces ; n++){
    lcd.print(" ");
  }
}

String completeLcdString(String str, int longRenglon){
  int spaces = longRenglon-str.length();
  for (int n = 0; n< spaces;n++){
    str += " ";
  }
  return str;
}

String centerLcdString(String str,int longReng){//devuelve un String con el str centrado en el renglon de largo longRenglon
  String result ="";
  int spaces = longReng-str.length();
  int first= spaces/2;
  for (int n =0; n < first; n++){
    result+=" ";
  }
  result+=str;
  return completeLcdString(result,longReng);
}

String opConfAnt(int opconf){
  return opConf(opconf,-1);
 }

 String opConfPost(int opconf){
  return opConf (opconf,1 );
 }

 String opConf(int opconf, int indice){
  if (opconf + indice < 1){
    return opcionesConf[opcionesConf_len-1];
  }else if ( opconf + indice > opcionesConf_len -1){
    return opcionesConf[0];
  }else{
    return opcionesConf[opconf + indice - 1];
  }
 }
String lcdPrintInManyRows(String str, int longReng, int row ){
  if (str.length() > longReng*(row-1)){  // si el string no entró en el renglon anterior
    if ( str.length() >longReng*row){ //si el string no termina en este renglon
        return str.substring(longReng*(row-1),longReng*row);  //se devuelven todos los caracteres que entran
    }else{  //si el string termina en este renlgon
        return completeLcdString(str.substring(longReng*(row-1)),longReng); //se devuelve la ultima parte del string
    }
  }else{
  //si el string se termino en el renglon anterior devuelve el string vacio
        return completeLcdString(" ", longReng);
  }
}


void config_printer(void){
  Serial.println(F("Iniciando Impresora:"));
  Serial.println(printer.resetPrinter());
  delay(100);
  printer.detectorSetting(ALL_TYPE);
  printer.setUnit(UNIT_MILLS);
  printer.setSpeed(1);
  printer.setASCIIGroup(SPANISH);
}
